package com.demo.mail.service;

import org.springframework.stereotype.Service;

import com.demo.mail.data.Mail;
import com.demo.mail.data.Order;


@Service
public interface MailService {
	public void sendMail(Mail mail);
	public void createAndSendMail(Order order);

}
