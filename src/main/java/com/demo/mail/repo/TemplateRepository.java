package com.demo.mail.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.demo.mail.data.NotificationTemplate;


@Repository
public interface TemplateRepository extends MongoRepository<NotificationTemplate, String> {
	public NotificationTemplate findByTemplateName(String templateName);
	public NotificationTemplate findById(String id);
}