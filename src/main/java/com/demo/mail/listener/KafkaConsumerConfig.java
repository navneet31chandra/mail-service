package com.demo.mail.listener;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;

import com.demo.mail.utils.PropertiesUtil;



@Configuration
@EnableKafka
public class KafkaConsumerConfig {
	protected final static Logger logger = LoggerFactory.getLogger(KafkaConsumerConfig.class);
	@Bean

    public Map consumerConfigs() {
        Map props = new HashMap<>();
        logger.info("Consumer Connected to the port "+PropertiesUtil.getProperty("server_address"));
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,PropertiesUtil.getProperty("server_address"));
        props.put(ConsumerConfig.GROUP_ID_CONFIG,PropertiesUtil.getProperty("group_id"));
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,true);
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG,PropertiesUtil.getProperty("auto_commit_interval"));
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG,PropertiesUtil.getProperty("session_timeout_interval"));
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG,PropertiesUtil.getProperty("max.poll.records"));
        props.put(ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG,PropertiesUtil.getProperty("fetch.message.max.bytes"));
        return props;
    }

    @Bean
    public ConsumerFactory consumerFactory() {
        return new DefaultKafkaConsumerFactory<Integer,String>(consumerConfigs());
    }

    @Bean
    public Receiver receiver() {
        return new Receiver();
    }

    @Bean
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<Integer, String>> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<Integer, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.setConcurrency(3);
        factory.getContainerProperties().setPollTimeout(3000);
        return factory;
    }

}
