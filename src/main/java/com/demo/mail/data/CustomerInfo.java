package com.demo.mail.data;

import lombok.Data;

import org.springframework.data.annotation.Id;


@Data
public class CustomerInfo {
	@Id
	private String id;
	private String email;
	private String mobileNo ;
	private String name;
}
