package com.demo.mail.data;

import lombok.Data;

import org.springframework.data.annotation.Id;


@Data
public class InventoryItem {
	@Id
	private String InvId;
	private String Brand;
	private double Mrp;
	private double discount ;
	private Integer orderQuantity;

}
