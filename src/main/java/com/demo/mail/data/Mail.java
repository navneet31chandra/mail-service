package com.demo.mail.data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import lombok.Data;

import org.springframework.data.mongodb.core.index.Indexed;

@Data
public class Mail implements Serializable {

    /** The Constant serialVersionUID. */
   // private static final long serialVersionUID = 123456789;

    /** The to list. */
    @NotNull
    @Indexed
    private List<String> toList;

    /** The ccList. */
    private List<String> ccList;

    /** The bccList. */
    private List<String> bccList;

    /** The body. */
    private Map<String, Object> body;

    /** The template name. */
    @NotNull
    @Indexed
    private String templateName;

    /** The subject. */
    @NotNull
    private String subject;

    /** The sender id. */
    @NotNull
    private String senderID;

    /** The priority. */
    private Integer priority;

    /** The attachmentObj. */
    private List<Attachment> attachmentObj;

    private String replyTo;

    /** The updateFromHeader. If set to true then mail from header name and reply to will be updated to the given senderID */
    private boolean updateFromHeader = false;

}

