package com.demo.mail.data;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

@Document(collection = "NotificationTemplates")
@Data
public class NotificationTemplate implements Serializable {

    private static final long serialVersionUID = -3662037931194745049L;
    @org.springframework.data.annotation.Id
    private String id;
    @Indexed(unique = true)
    @NotNull
    private String templateName;
    @NotNull
    private String templateContent;
    private String templateType;
    private String templateKeys;
    @DateTimeFormat
    private Date updatedDate;
    private String updatedBy;

}
