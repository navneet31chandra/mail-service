package com.demo.mail.data;

import lombok.Data;

@Data
public class Attachment {
	private byte[] fileByteArray;
    private String fileName;
    private String fileType;

}
