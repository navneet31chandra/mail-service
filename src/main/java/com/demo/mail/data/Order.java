package com.demo.mail.data;

import java.util.List;

import lombok.Data;

import org.springframework.data.annotation.Id;



@Data
public class Order {
	@Id
	private String id;
	private CustomerInfo custInfo;
	private List<InventoryItem> invItems ;
	private InvoiceInfo invoiceInfo ;

}
