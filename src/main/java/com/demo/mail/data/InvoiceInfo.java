package com.demo.mail.data;

import java.util.List;

import lombok.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;


@Data
public class InvoiceInfo {
	@Id
	String id;
	@Indexed(background = true)
	String orderID;
	List<InventoryItem> invItems;
	private double cgstPerstange;
	private double sgstPerstange;
	private double igstPerstange;
	private double totalCgst;
	private double totalSgst;
	private double totalIgst;
	private double fullMrp ;
	private double discount ;
	private double payableAmount;
	

}