package com.demo.mail.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
@Controller
@EnableWebMvc
@RequestMapping("/api/mail")
public class MailController {
	@RequestMapping(value="/isUp",method = RequestMethod.GET)
    public @ResponseBody
    boolean getHealth(){
        return true;
    }
}
