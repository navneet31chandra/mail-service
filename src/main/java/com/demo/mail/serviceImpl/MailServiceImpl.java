package com.demo.mail.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.demo.kafkaQ.constants.KafkaServiceTopic;
import com.demo.kafkaQ.service.KafkaQService;
import com.demo.mail.data.CustomerInfo;
import com.demo.mail.data.Mail;
import com.demo.mail.data.Order;
import com.demo.mail.service.MailService;

public class MailServiceImpl implements MailService {
	@Autowired
	private KafkaQService kafkaQService;

	@Override
	public void sendMail(Mail mail) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createAndSendMail(Order order) {
		Mail mail ;
		try{
		if(order.getInvoiceInfo() != null){
			 mail = prepareMailWithInvoice(order);
		}
		else{
		 mail = prepareMailWithoutInvoice(order);
		}
		
		sendMail(mail);} catch(Exception e){
			kafkaQService.insert(order, KafkaServiceTopic.MAILTOPIC);
		}
		
	}

	private Mail prepareMailWithoutInvoice(Order order) {
		Mail mail = new Mail();
		CustomerInfo custInfo = order.getCustInfo();
		String customerMailId = custInfo.getEmail();
		String orderId = order.getId();
		String CustomerName = custInfo.getName();
		List<String> toList = new ArrayList<String>();
		toList.add(customerMailId);
		mail.setToList(toList);
		return mail;
	}

	private Mail prepareMailWithInvoice(Order order) {
        Mail mail = new Mail();
        CustomerInfo custInfo = order.getCustInfo();
		String customerMailId = custInfo.getEmail();
		String orderId = order.getId();
		String CustomerName = custInfo.getName();
		List<String> toList = new ArrayList<String>();
		toList.add(customerMailId);
		mail.setToList(toList);
		return mail;
	}

}
